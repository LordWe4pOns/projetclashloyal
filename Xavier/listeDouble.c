#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "listeDouble.h"

void initListe(T_liste *l){
    *l = NULL;
}


bool listeVide(T_liste l){
    return (l == NULL);
}

/*
void afficheListeV1(T_liste l){
    T_liste courant = l;
    while (!listeVide(courant)){
        printf(" %d ",*(courant->pdata));
        courant=courant->suiv;
    }
}*/


T_liste ajoutEnTete(T_liste l, Tunite mydata){

    // Allocation pour la nouvelle cellule
    T_liste new_cell = (T_liste)malloc(sizeof(struct T_cell));

    // Allocation pour le pointeur pdata
    new_cell->pdata = (Tunite*)malloc(sizeof(Tunite));
    *(new_cell->pdata) = mydata;

    new_cell->prec = NULL;
    new_cell->suiv = l;
    if (!listeVide(l)){
    l->prec = new_cell;
    }

    return new_cell;
}

T_liste ajoutEnFin(T_liste l, Tunite mydata){

    // Allocation pour la nouvelle cellule
    T_liste new_cell = (T_liste)malloc(sizeof(struct T_cell));

    // Allocation pour le pointeur pdata
    new_cell->pdata = (Tunite*)malloc(sizeof(Tunite));
    *(new_cell->pdata) = mydata;

    new_cell->suiv = NULL;

    T_liste courant = l;

    //si l est vide
    if (listeVide(courant)){
        new_cell->prec = NULL;
        return new_cell;        // on renvoie la nouvelle cellule, car c'est la seule
    }
    else{   // si l n'est pas vide, on change la valeur suivante de la derni�re cellule de l (= new_cell)
            // et la valeur prec de la nouvelle valeur = la derni�re cellule de l
        T_liste last_cell = getptrLastCell(courant);
        new_cell->prec = last_cell;
        last_cell->suiv = new_cell;
        return courant;
    }
}

T_liste ajoutEnN(T_liste l, int pos, Tunite mydata){

    // Allocation pour la nouvelle cellule
    T_liste new_cell = (T_liste)malloc(sizeof(struct T_cell));

    // Allocation pour le pointeur pdata
    new_cell->pdata = (Tunite*)malloc(sizeof(Tunite));
    *(new_cell->pdata) = mydata;

     if (pos == 0) {
        // Ajout en t�te
        return ajoutEnTete(l, mydata);
    }
    else {
        T_liste courant = l;
        int i = 0;

        // Parcourir jusqu'� la position (pos N) ou la fin de la liste
        while (i < pos - 1 && courant != NULL) {
            courant = courant->suiv;
            i++;
        }

        if (listeVide(courant)) {
            // Ajout en fin, car pos > nbr �l�ment liste
            return ajoutEnFin(l, mydata);
        }
        else {
            // Insertion � la position sp�cifi�e
            new_cell->suiv = courant->suiv;
            new_cell->prec = courant;

            // Changement de la valeur 'suiv' de la cellule pr�c�dente (N-1)
            courant->suiv = new_cell;

            //Changement de la valeur 'prec' de la cellule suivante (N+1)
            if (!listeVide(courant->suiv)) {       // si diff�rent de null
                courant->suiv->prec = new_cell;
            }
        return l;
        }
    }
}


T_liste suppEnTete(T_liste l){
    T_liste courant = l->suiv;

    if(!listeVide(l)){      // Si la liste n'est pas vide
        free(l->pdata);         // lib�re les espaces allou�s par la premi�re cellule
        free(l);                //  " = "
        courant->prec = NULL;   // nouvelle premi�re valeur = prec � NULL
        return courant;         // renvoie nouvelle premi�re valeur
    }
    else{
        return l;   // Si la liste est vide (renvoie une liste vide / NULL)
    }
}

T_liste suppEnFin(T_liste l){

    if(!listeVide(l)){      // Si la liste n'est pas vide
        T_liste courant = l;
        if(listeVide(courant->suiv)){    //Si la liste n'a qu'un �l�ment
            free(courant->pdata);
            free(courant);
            return NULL;           // renvoie une cellule vide
        }
        else{
            // Parcour la liste jusqu'� l'avant derni�re valeur
            while(!listeVide(courant->suiv->suiv)){
                courant = courant->suiv;
            }

            free(courant->suiv->pdata);
            free(courant->suiv);
            courant->suiv = NULL;
            return l;         // renvoie la premi�re valeur (inchang� sauf si la liste n'avait qu'une cellule)
        }
    }
    else{
        return l;   // Si la liste est vide (renvoie une liste vide / NULL)
    }
}

T_liste suppEnN(T_liste l, int pos){

    // On test si la liste en entr�e est vide ou la valeur de n est n�gative
    if(listeVide(l) || pos < 0){
        return l;     //renvoie une liste (NULL)
    }
    else{

        //si on veut supprimer la premi�re valeur (pos = 0)
        if(pos == 0){

            T_liste temp = l->suiv;

            //�quivalent � suppEnTete -----------------------------
            if(temp != NULL){
                temp->prec = NULL;
            }

            free(l->pdata);
            free(l);
            return temp;
            //--------------------------
        }
        else{
            T_liste courant = l;
            int i = 0;
            // On parcour la liste jusqu'� la pos voulu et tant que la liste n'est pas vide
            while (!listeVide(courant) && i < pos){
                courant=courant->suiv;
                i++;
            }

            // la position est en dehors de la liste
            if(courant == NULL){
                printf("La position de la valeur � supprimer est en dehors des limites de la liste");
                return l;
            }

            //changement pour la cellule pr�c�dente (modification de la cell suiv (case � supprimer))
            courant->prec->suiv = courant->suiv;

            //changement pour la cellule suivante (modification de la cell prec (case � supprimer))
            //Si elle existe (si la case � supprimer n'est pas la derni�re valeur)
            if(!listeVide(courant->suiv)){
                courant->suiv->prec = courant->prec;
            }

            //Supprime la cellule courante qui est sur la valeur pos
            free(courant->pdata);
            free(courant);

            return l;
        }
    }
}

T_liste getptrFirstCell(T_liste l){
    // liste vide
    if(listeVide(l)){
        printf("\n ptrFistCell : liste vide.\n");
        return l;
    }
    else{

        T_liste courant = l;

        // courant ou l est la d�j� la premi�re cellule de la liste
        if(listeVide(l->prec)){
            return l;
        }
        else{
            // On parcoure la liste jusqu'� la premi�re cellule
            while (!listeVide(courant->prec)){
                courant=courant->prec;
            }

            return courant;
        }
    }
}

T_liste getptrLastCell(T_liste l){
    // liste vide
    if(listeVide(l)){
        printf("\n ptrLastCell : liste vide.\n");
        return l;
    }
    else{

        T_liste courant = l;

        // courant ou l est la d�j� la derni�re cellule de la liste
        if(listeVide(l->suiv)){
            return l;
        }
        else{
            // On parcour la liste jusqu'� la derni�re cellule
            while (!listeVide(courant->suiv)){
                courant = courant->suiv;
            }

            return courant;
        }
    }
}

T_liste getptrNextCell(T_liste l){

    T_liste courant = l;
    // liste vide
    if(listeVide(courant)){
        printf("\n getptrNextCell : liste vide.\n");
        return l;
    }
    else{
        // liste n'a pas de valeur suivante (l = derni�re cellule)
        if(listeVide(courant->suiv)){
            printf("\n getptrNextCell : il n'y a pas de valeur suivante � cette cellule.\n");
            return courant->suiv;
        }
        else{
            return courant->suiv;
        }
    }
}

T_liste getptrPrevCell(T_liste l){

    T_liste courant = l;
    if(listeVide(courant)){
            // liste vide
        printf("\n getptrPrevCell : liste vide.\n");
        return l;
    }
    else{
        // liste n'a pas de valeur pr�c�dente ( l = premi�re cellule)
        if(listeVide(courant->prec)){
            printf("\n getptrPrevCell : il n'y a pas de valeur pr�c�dente � cette cellule.\n");
            return courant->prec;
        }
        else{
            return courant->prec;
        }
    }
}


Tunite* getPtrData(T_liste l){
    // liste vide
    if(listeVide(l)){
        printf("\n ERREUR getPtrData: liste vide.\n");
        return NULL;        // EXIT_FAILURE
    }
    else{
        return l->pdata;
    }
}


int getNbreCell(T_liste l){
    // liste vide
    if(listeVide(l)){
        return 0;
    }
    else{
        // on va � la premi�re valeur si ce n'est pas celle-ci en param�tre
        T_liste courant = getptrFirstCell(l);
        int c = 0;

        // On parcoure la liste jusqu'� la derni�re valeur et on compte le nombre de cellule avec c
        while (!listeVide(courant)){
            courant=courant->suiv;
            c++;
        }

        return c;
    }
}


void swapPtrData( T_liste source, T_liste destination ){
    if(listeVide(source) || listeVide(destination)){
        printf("ERREUR swapPtrData : une des deux liste est vide.\n");
    }
    else{
        Tunite *temp = source->pdata;
        source->pdata = destination->pdata;
        destination->pdata = temp;
    }
}

int getSizeBytes(T_liste l){
    if (listeVide(l)) return 0;
    int size;
    size = sizeof(T_liste) * getNbreCell(l);
    return size;
}

T_liste creatNewListeFromFusion(T_liste l1, T_liste l2){

    //on alloue de la place pour une nouvelle liste
    T_liste new_L;
    initListe(&new_L);

    // on ajoute les valeurs de la liste l1 (en fin) tant que la liste n'est pas vide
    T_liste courant_L1 = l1;
    while(!listeVide(courant_L1)){
        new_L = ajoutEnFin(new_L, *(courant_L1->pdata));
        courant_L1 = courant_L1->suiv;
    }

    // on ajoute les valeurs de la liste l2 (en fin) tant que la liste n'est pas vide
    T_liste courant_L2 = l2;
    while(!listeVide(courant_L2)){
        new_L = ajoutEnFin(new_L, *(courant_L2->pdata));
        courant_L2 = courant_L2->suiv;
    }

    return new_L;
}

T_liste addBehind(T_liste debut, T_liste suite){

    if(listeVide(debut)){
        return suite;      // la liste l1 est vide donc on renvoie l2
    }
    else{
        if(listeVide(suite)){
            return debut; // la liste l2 est vide donc on renvoie l1
        }
        else{
            T_liste last_cell_L1 = getptrLastCell(debut);      // on r�cup�re la derni�re cellule de la liste 11
            T_liste first_cell_L2 = getptrFirstCell(suite);    // on r�cup�re la premi�re cellule de la liste l2

            //on colle la derni�re cellule de l1 � la premi�re de l2
            last_cell_L1->suiv = first_cell_L2;
            //et inversement
            first_cell_L2->prec = last_cell_L1;

            return debut; // on renvoie l1 (qui contient maintenant l1 + l2 � la suite)
        }
    }
}

/*

T_liste findCell(T_liste l, TuniteDuJeu nom){
    T_liste courant = l;
    bool find = false;

    while(!listeVide(courant) || find){
        if ((*(courant->pdata)).nom == nom){
            find = true;
        }else{
            courant = courant->suiv;
        }
    }

    return courant;
}*/

int getOccurences(T_liste l, Tunite data, bool (*comp)(Tunite a, Tunite b)){
    T_liste courant = l;
    int i, n=0, size = getNbreCell(l);
    for (i = 0; i<size; i++){
        if (comp(data, *(courant->pdata)))
            n++;
        courant = courant->suiv;
    }
    return n;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// FIN Fonctions listes //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Getter //////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

TuniteDuJeu get_nom(T_liste l){
    return l->pdata->nom;
}
Tcible get_cibleAttaquable(T_liste l){
    return l->pdata->cibleAttaquable;
}
Tcible get_maposition(T_liste l){
    return l->pdata->maposition;
}
int get_pointsDeVie(T_liste l){
    return l->pdata->maposition;
}
float get_vitesseAttaque(T_liste l){
    return l->pdata->vitesseAttaque;
}
int get_degats(T_liste l){
    return l->pdata->degats;
}
int get_portee(T_liste l){
    return l->pdata->portee;
}
float get_vitesseDeplacement(T_liste l){
    return l->pdata->vitessedeplacement;
}
int get_posX(T_liste l){
    return l->pdata->posX;
}
int get_posY(T_liste l){
    return l->pdata->posY;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Fin Getter ////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Debut Setter ////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void set_nom(T_liste l, TuniteDuJeu new_nom){
    l->pdata->nom = new_nom;
}
void set_cibleAttaquable(T_liste l, Tcible new_cible){
    l->pdata->cibleAttaquable = new_cible;
}
void set_maposition(T_liste l, Tcible new_pos){
    l->pdata->maposition = new_pos;
}
void set_pointsDeVie(T_liste l, int new_pdv){
    l->pdata->pointsDeVie = new_pdv;
}
void set_vitesseAttaque(T_liste l, float new_atkspeed){
    l->pdata->vitesseAttaque = new_atkspeed;
}
void set_degats(T_liste l, int new_degats){
    l->pdata->degats = new_degats;
}
void set_portee(T_liste l, int new_portee){
    l->pdata->portee = new_portee;
}
void set_vitesseDeplacement(T_liste l, float new_vitesseDeplacement){
    l->pdata->vitessedeplacement = new_vitesseDeplacement;
}
void set_posX(T_liste l, int new_posX){
    l->pdata->posX = new_posX;
}
void set_posY(T_liste l, int new_posY){
    l->pdata->posY = new_posY;
}
void set_peutAttaquer(T_liste l, int new_value){
    l->pdata->peutAttaquer = new_value;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Fin Setter ////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/
