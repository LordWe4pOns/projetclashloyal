
Commandes Git-Gitlab :

- Pour ouvrir la console : *Clic droit* dans le répertoire -> Git Bash Here (il faut avoir téléchager git)

- Copier les documents sur son ordinateur : git clone https://gitlab.com/Xavier_/projetclashloyal.git


- Pour mettre à jour le répertoire : git pull


- Envoyer son travail sur git :

	1:	git add NOM_DU_FICHIER/DOSSIER (tab pour l'autocomplétion).
		ou git add . (pour tout ajouter)

	2:	git commit -m "UN COMMENTAIRE.. NOM DE LA PERSONNE / DATE..."

	3:	git push