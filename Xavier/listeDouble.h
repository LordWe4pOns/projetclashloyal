#ifndef LISTEDOUBLE_H_INCLUDED
#define LISTEDOUBLE_H_INCLUDED
#include <stdbool.h>
#include "types.h"


void initListe(T_liste* l);
bool listeVide(T_liste l);
void afficheListeV1(T_liste l);

//Ajout valeur
T_liste ajoutEnTete(T_liste l, Tunite mydata);
T_liste ajoutEnFin(T_liste l, Tunite mydata);
T_liste ajoutEnN(T_liste l, int pos, Tunite mydata);

//Del valeur
T_liste suppEnTete(T_liste l);
T_liste suppEnFin(T_liste l);
T_liste suppEnN(T_liste l, int pos);

// GET Pointeur
T_liste getptrFirstCell(T_liste l);
T_liste getptrLastCell(T_liste l);
T_liste getptrNextCell(T_liste l);
T_liste getptrPrevCell(T_liste l);


Tunite* getPtrData(T_liste l);
int getNbreCell(T_liste l);
void swapPtrData( T_liste source, T_liste destination );
int getSizeBytes(T_liste l);

//Fusion liste
T_liste creatNewListeFromFusion(T_liste l1, T_liste l2);
T_liste addBehind(T_liste debut, T_liste suite);

int getOccurences(T_liste l, Tunite data, bool (*comp)(Tunite a, Tunite b));

// Getter
TuniteDuJeu get_nom(T_liste l);
Tcible get_cibleAttaquable(T_liste l);
Tcible get_maposition(T_liste l);
int get_pointsDeVie(T_liste l);
float get_vitesseAttaque(T_liste l);
int get_degats(T_liste l);
int get_portee(T_liste l);
float get_vitesseDeplacement(T_liste l);
int get_posX(T_liste l);
int get_posY(T_liste l);

// Setter
void set_nom(T_liste l, TuniteDuJeu new_nom);
void set_cibleAttaquable(T_liste l, Tcible new_cible);
void set_maposition(T_liste l, Tcible new_pos);
void set_pointsDeVie(T_liste l, int new_pdv);
void set_vitesseAttaque(T_liste l, float new_atkspeed);
void set_degats(T_liste l, int new_degats);
void set_portee(T_liste l, int new_portee);
void set_vitesseDeplacement(T_liste l, float new_vitesseDeplacement);
void set_posX(T_liste l, int new_posX);
void set_posY(T_liste l, int new_posY);
void set_peutAttaquer(T_liste l, int new_value);


#endif // LISTEDOUBLE_H_INCLUDED
